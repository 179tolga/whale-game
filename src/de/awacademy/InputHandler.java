package de.awacademy;
import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

//Steuerung für den Spieler
public class InputHandler {

    private Model model;
    public InputHandler (Model model) {
        this.model = model;
    }
    public void onKeyPressed(KeyCode keycode) {

        if(keycode == KeyCode.UP) {
            model.getPlayer().move(0, -5);
        }
        if(keycode == KeyCode.DOWN) {
            model.getPlayer().move(0, 5);
        }
        if(keycode == KeyCode.LEFT) {
            model.getPlayer().move(-10, 0);
        }
        if(keycode == KeyCode.RIGHT) {
            model.getPlayer().move(10, 0);
        }
    }
}
