package de.awacademy;
import java.io.FileInputStream;
import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javax.swing.*;
import java.io.FileNotFoundException;

public class Graphics<coll> extends JFrame {

    private GraphicsContext gc;
    public Model model;

    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }

    // Fügt Bilder für die Objekte und den Hintergrund hinzu, benötigt Exception!
    Image hint = new Image(new FileInputStream("src/de/awacademy/images/hintergrund.jpg"));
    Image ship = new Image(new FileInputStream("src/de/awacademy/images/ship.png"));
    Image wal = new Image(new FileInputStream("src/de/awacademy/images/wal.png"));

    // Draw Methode erzeugt die Objekte
    public void draw() {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);
        gc.drawImage(hint, 0, 0, model.WIDTH, model.HEIGHT);
        gc.drawImage(ship, model.getPlayer().getX(), model.getPlayer().getY(), model.getPlayer().getW(), model.getPlayer().getH());
        gc.drawImage(wal, model.getEnemy().getX(), model.getEnemy().getY(), model.getEnemy().getW(), model.getEnemy().getH());
    }
}


