package de.awacademy.model;

//Erzeugt den Gegner
public class Enemy {
    private int x;
    private int y;
    private int h;
    private int w;

    // Hier wird die Anfangsgröße festgelegt
    public Enemy(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 50;
        this.w = 100;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public int getX() {
        if (x < 400)
            return x;
        else {
            x = 0;
            return x;
        }
    }

    public int getY() { //Gegner wird zufällig von Oben nach Unten gesendet
        if (y < 380)
            return y;
        else {
            y = 0;
            x = (int)(Math.random()*300);
            w = 20 + w; //Gegner wird immer größer
            h = 10 + h;
            return y;
        }
    }
    public int getH() {
        return h;
    }
    public int getW() {
        return w;
    }
}
