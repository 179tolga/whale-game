package de.awacademy.model;

//Erzeugt den Spieler
public class Player {

    private int x;
    private int y;
    private int h;
    private int w;

    //Größe des Spielers wird hier festgelegt
    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 80;
        this.w = 60;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public int getX() {
        if (x>0 && x < 390) //Spieler soll Spielfeld nicht verlassen
            return x;
        else {
            x = 0;
            return x;
        }
    }

    public int getY() { //wenn Spieler nach unten "fällt" kommt er von oben wieder
        if (y < 390)
            return y;
        else {
            y = 200;
            return y;
        }
    }
    public int getH() {
        return h;
    }
    public int getW() {
        return w;
    }
}
