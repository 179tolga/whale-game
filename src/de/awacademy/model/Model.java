package de.awacademy.model;

public class Model {
    private Player player;
    private Enemy enemy;
    public boolean collision = false;
    public final double WIDTH = 400;
    public final double HEIGHT = 400;

    public Model() {
        this.player = new Player(200, 300);
        this.enemy = new Enemy(((int) (Math.random() * 100)), 0);
    }

    public Player getPlayer() {
        return player;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public boolean isCollision() {
        return collision;
    }

    public void update(long nowNano) {
        while (collision) {
            //Code zum Beenden
            System.out.println("Game Over");
        }

        // Enemy bewegt sich langsam runter
        enemy.move(0, (int) (Math.random() * 3));

        // Collision-detector
        if (((enemy.getX() > player.getX()) && (enemy.getX() < player.getX() + player.getW()) || ((player.getX() < enemy.getX() + enemy.getW()) && (player.getX() > enemy.getX())))
                && Math.abs(player.getY() - enemy.getY()) < enemy.getH()) {
            collision = true;
        }
    }
}
