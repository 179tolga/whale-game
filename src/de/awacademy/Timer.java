package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    public Graphics graphics;
    private Model model;

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {
        model.update(nowNano);
        graphics.draw();
            }
   }
